import socket
import sys
import os
import math

path = str(sys.argv[1])
host = sys.argv[2] 
port = int(sys.argv[3])

s = socket.socket() 
s.connect((host, port))

f = open(path,'rb')
file_size = os.stat(path).st_size
chunk_size = math.floor(file_size/10)
if (not chunk_size): 
    chunk_size = 1
print(chunk_size)
chunk_cnt = 0

s.send(str(path + '\n').encode())
l = f.read(chunk_size)
while (l):
    print ('Sent ' + str(chunk_cnt*100/(file_size/chunk_size)) + '% of the file...')
    s.send(l)
    l = f.read(chunk_size)
    chunk_cnt += 1
f.close()
print ("Sent 100% of the file...")
s.shutdown(socket.SHUT_WR)
s.close   