import socket
import os
import sys

s = socket.socket()
host = '0.0.0.0'
port = int(sys.argv[1])
s.bind((host, port))
s.listen(5)                 
while True:
    c, addr = s.accept()
    print ('Got connection from', addr)

    filename = ""
    while True:
        d = c.recv(1).decode('ascii')
        if d == '\n':
            break
        filename += d

    if (not os.path.isfile('./' + filename)):
        f = open(filename,'wb')
    else:
        i = 1
        while True:
            if (os.path.isfile('./' + filename.split('.')[0] + '_copy' + str(i) + '.' + filename.split('.')[1])):
                i += 1
            else:
                f = open(filename.split('.')[0] + '_copy' + str(i) + '.' + filename.split('.')[1],'wb')
                break

    l = c.recv(1024)
    while (l):
        f.write(l)
        l = c.recv(1024)
    f.close()
    print ("Done Receiving")
    c.close()
